import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private url = 'http://0.0.0.0/api/files';

  constructor(
    private http: HttpClient,
  ) { }

  public upload(payload: any, base64File: any): Observable<any> {
    payload.encodedFile = base64File;

    return this.http.post(this.url, payload);
  }

  public get(currPage = 1): Observable<any> {
    return this.http.get(`${this.url}?page=${currPage}`);
  }

  public getById(id: string): Observable<any> {
    return this.http.get(`http://0.0.0.0/api/file/${id}`);
  }

  public patch(id: string, payload: any, base64File: any): Observable<any> {
    payload.encodedFile = base64File;

    return this.http.patch(`${this.url}/${id}`, payload);
  }
}
