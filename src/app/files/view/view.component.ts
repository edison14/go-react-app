import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FileService } from '../../services/file.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.sass']
})
export class ViewComponent implements OnInit {

  public id;

  public file;
  public ext;

  constructor(
    private fileService: FileService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
      if (this.id) {
        this.fileService.getById(this.id).subscribe((result) => {
          this.file = result;
          this.ext  = result.url.split('.').pop();
        }, () => {
          this.router.navigateByUrl('/list');
        });
      } else {
        this.router.navigateByUrl('/list');
      }
    });
  }

}
