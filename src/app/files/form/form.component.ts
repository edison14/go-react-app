import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FileService } from '../../services/file.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

  public submitted = false;

  public id = null;

  public loading = false;

  public base64File = {
    ext: '',
    encoded: null
  };

  public errors;

  public fileForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    file: new FormControl('', Validators.required)
  });

  constructor(
    private fileService: FileService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.id = params.get('id');
      if (this.id) {
        this.loading = true;
        this.fileService.getById(this.id).subscribe((result) => {
          this.loading = false;
          this.fileForm.setValue({
            title: result.title,
            description: result.description,
            file: ''
          });

          this.base64File.ext = result.url.split('.').pop();
          this.base64File.encoded = result.url;

          // tslint:disable-next-line: no-string-literal
          this.fileForm.controls['file'].setValidators([]);
          // tslint:disable-next-line: no-string-literal
          this.fileForm.controls['file'].updateValueAndValidity();

        }, (error) => {
          this.router.navigateByUrl('/list');
          this.loading = false;
        });
      }
    }, () => {
    });
  }

  /**
   * Handles on submit.
   */
  public onSubmit(): void {
    this.submitted = true;

    if (this.fileForm.valid === true) {
      this.loading = true;

      if (this.id) {
        this.fileService.patch(this.id, this.fileForm.value, this.base64File).subscribe((result) => {
          this.loading = false;
          this.router.navigateByUrl(`view/${result.id}`);
        }, (errors) => {
          this.errors = errors;
          this.loading = false;
        });
      } else {
        this.fileService.upload(this.fileForm.value, this.base64File).subscribe((result) => {
          this.loading = false;
          this.router.navigateByUrl(`view/${result.id}`);
        }, (errors) => {
          this.errors = errors;
          this.loading = false;
        });
      }
    }
  }

  public onSelectFile(event): void {
    const file = event.target.files[0];

    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.base64File.ext = file.name.split('.').pop();
        this.base64File.encoded = reader.result;
      };
    }
  }
}
