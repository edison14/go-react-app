import { Component, OnInit } from '@angular/core';
import { FileService } from '../../services/file.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public files: any;

  public currPage = 1;
  public itemsPerPage = 5;
  public total = 0;
  public pagination;

  public loading = false;

  public theads = [
    {
      text: 'Title',
    },
    {
      text: 'Description',
    },
    {
      text: '',
    }
  ];

  constructor(
    private fileService: FileService,
    private router: Router
  ) {
    this.files = [];
  }

  ngOnInit(): void {
    this.loadFiles();
  }

  public loadFiles(): void {
    this.loading = true;
    this.fileService.get(this.currPage).subscribe((result) => {
      this.pagination = result;
      this.files = this.pagination.data;
      this.total = this.pagination.total;
      this.loading = false;
    }, () => {
      this.files = [];
      this.currPage = 1;
      this.total = 0;
      this.loading = false;
    });
  }

  public onPageChange(page): void {
    this.currPage = page;
    this.loadFiles();
  }

  public toEdit(file): void {
    this.router.navigateByUrl(`form/${file.id}`);
  }

  public toView(file): void {
    this.router.navigateByUrl(`view/${file.id}`);
  }
}
