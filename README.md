# GoReactApp

This project is a sample uploading and viewing of MP4 and JPG files.

## Setup

### Please install the following:
Node v12.11.1
NPM 6.11.3
Angular @angular/cli

### Running your local server
* Run `npm install` to install dependencies.
* Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of     the source files.